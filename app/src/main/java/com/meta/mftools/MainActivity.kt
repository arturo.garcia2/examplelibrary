package com.meta.mftools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.meta.metatools.MetaToolsDates

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val time = MetaToolsDates.getTimestampId()
        Log.i("El tiempo es ", time)
    }
}