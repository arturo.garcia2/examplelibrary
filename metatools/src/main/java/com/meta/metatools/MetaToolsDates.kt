package com.meta.metatools

import android.text.format.DateFormat
import java.util.*
import kotlin.random.Random

class MetaToolsDates {

    companion object {
        fun getTimestampId(): String {
            val date = Date()
            val timestamp = DateFormat.format("_yyyy-MM-dd_HH:mm:ss_", date).toString()

            return timestamp.plus(randomValues.first())
        }

        private val randomValues = List(1) {
            Random.nextInt(0, 100)
        }
    }



}